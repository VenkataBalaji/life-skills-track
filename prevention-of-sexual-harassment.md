# Sexual Harassment #
 Sexual harassment refers to unwelcome or inappropriate behavior of a sexual nature that creates an intimidating, hostile, or offensive environment. This can include unwanted advances, comments, gestures, or actions that make a person feel uncomfortable, threatened, or violated based on their gender or sexual orientation.
 # What to Do in Case of Sexual Harassment

If you face or witness any incident or repeated incidents of sexual harassment or inappropriate behavior, follow these steps:

1. **Document the Incident**: Take note of specific details regarding the time, location, individuals involved, and the nature of the behavior.

2. **Report to Authorities**: Immediately report the incident to the appropriate authorities within the organization or institution.

3. **Seek Support**: Reach out to trusted colleagues, friends, or counselors for emotional support and guidance.

4. **Follow Internal Procedures**: Follow the established procedures and protocols within your organization for addressing sexual harassment.

5. **Document Responses**: Keep a record of any responses or actions taken by the authorities in response to your report.

6. **Raise Awareness**: Advocate for awareness and prevention of sexual harassment by promoting education and discussions.

7. **Know Your Rights**: Familiarize yourself with your rights and protections under relevant laws and policies.

8. **Support Others**: Offer support and assistance to others who may have experienced or witnessed similar incidents.

It's important to address incidents of sexual harassment promptly and effectively to ensure a safe and respectful environment for everyone.

  * ## What kinds of behaviour cause sexual harassment? ##

 1. **Unwelcome Comments or Jokes:**  Making sexual comments, jokes, or innuendos that are unwelcome by the recipient.

1. **Unwanted Advances:** Making unwanted sexual advances, such as touching, hugging, kissing, or groping.

1. **Sexual Propositions:** Requesting sexual favors in exchange for employment benefits, promotions, grades, or other favorable treatment.

1. **Displaying Inappropriate Material:** Showing or displaying sexually explicit materials, such as images, videos, or texts, in the workplace or academic environment.

1. **Inappropriate Communication:** Sending unwanted sexually explicit emails, texts, or messages, including sexting.
1. **Dressing sence :** wearing the abnormal dressing and expose to themself's, behave to the taking the advantages for sexual harassment. 




* ## What would you do in case you face or witness any incident or repeated incidents of such behaviour? ##

1. Firstly, I try to avoid that particular person, and the set the limits in between of them.
1. **Set Boundaries:** Clearly establish your boundaries with the individual and make it clear what behavior is inappropriate. Be firm and consistent in enforcing these boundaries.
1. **Report the Incident:** I would report the incident to the appropriate authority within the organization or community, following any established reporting procedures or policies in place. This might involve HR personnel, a supervisor, community moderators.
1. **Use Available Resources:** I would utilize any available resources or support services provided by the organization or community, such as counseling services, employee assistance programs, or legal assistance if needed.

1. **Advocate for Change:** If necessary, I would advocate for changes to policies, procedures, or the organizational culture to prevent future incidents of sexual harassment and create a safer and more inclusive environment for everyone.

