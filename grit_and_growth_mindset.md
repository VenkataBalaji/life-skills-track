### Grit

* Angela Duckworth explores "grit," the combination of passion and perseverance for long-term goals. Grit, she argues, is more important than talent for success. People with grit stick with their goals despite challenges, fueled by a "growth mindset" that sees abilities as developable. Examples from education, business, and sports showcase grit's power. The talk concludes with tips for fostering grit in ourselves and others.


### Fixed Mindset:
 - People believe intelligence and skills are fixed traits. They shy away from challenges, view mistakes as failures, and resist feedback.
### Growth Mindset:
- Individuals believe skills and intelligence can be developed through effort. They embrace challenges as learning opportunities, see mistakes as natural steps in growth, and value feedback for improvement.



 ## What is the Internal Locus of Control? What is the key point in the video?


### Internal Locus of Control
Internal locus of control refers to the belief that your achievements and life outcomes are primarily determined by your own efforts, skills, and decisions. Individuals with a strong internal locus of control feel empowered and motivated because they perceive themselves as having control over their success.

### keypoints
 * The study fond that students who are prised their effort and hard work instead of their inteligence showed higher levels of motivation

 * students who were prised for their inteligence showed lower motivation and belived that external factors determined their succuss.

 * taking credit for the positive outcomes you achieve
 * Delevoping an internal locus of control is key to staying


 ### What are the key points mentioned by speaker to build growth mindset (explanation not needed).

* Believe that skills and inteligence can be developed through hard work and dedication

* see mistakes as a natural part of the learning process, not failures
* View challenges as opportunities to learn and grow, not roadblocks.
* Appreciate feedback as a way to identify areas for improvement.
* Set ambitious goals that push you outside your comfort zone.
* Don't give up easily; keep working towards your goals.


### What are your ideas to take action and build Growth Mindset?
  * take responsibility for your action 
  * putting efforts in the initial step for a growth mindset
  * embrace challenges, see mistakes as learning opportunities, and appreciate feedback