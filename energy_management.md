# Energy Management

## 1. Relaxation Activities (Calm Quadrant)
- Listening to music.
- Playing volleyball.
- Engaging in online games.

## 2. Stress Triggers (Stress Quadrant)
- Approaching deadlines.
- Long periods of continuous work.

## 3. Identifying Excitement (Excitement Quadrant)
- Positive mood and accelerated mental state.
- Enhanced productivity and efficiency.

## 4. Paraphrase of "Sleep is Your Superpower" Video
- Sleep is crucial, not optional.
- Proper sleep boosts the immune system and concentration.
- Establishing a consistent sleep schedule is essential.
- Deep sleep aids in solidifying temporary memories.
- Adequate sleep contributes to reducing heart problems.

## 5. Improving Sleep Quality
- Limit screen time before bedtime.
- Maintain a strict sleep schedule.
- Prioritize sleep over non-essential activities.

## 6. Paraphrase of "Brain Changing Benefits of Exercise" Video
- Reduces anxiety and depression.
- Enhances intelligence and cognitive abilities.
- Improves blood circulation to the brain.
- Increases focus and concentration.
- Stimulates brain cell growth.

## 7. Steps to Increase Exercise
- Incorporate exercise into daily routines.
- Join a supportive community.
- Set achievable weekly exercise goals.
