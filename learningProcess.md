# Feynman Technique ? #

The Feynman Technique is learning by explaining a concept as if teaching it to someone who knows nothing about it.

## In this video, what was the most interesting story or idea for you? ##
The TED Talk by Barbara Oakley explores learning methods. Based on the video length, the pinball machine analogy explaining different brain modes might be the most intriguing part. This analogy likely breaks down complex thinking into a simpler, relatable concept. Watch the TED Talk to see what sparks your curiosity the most!

## What are active and diffused modes of thinking? ##

The active mode of thinking involves focused, analytical, and linear processing of information, suitable for tasks that require concentration and logical reasoning. In contrast, the diffused mode of thinking is relaxed, creative, and associative, allowing for holistic exploration and idea generation. Both modes are important for learning and problem-solving, with the active mode being task-oriented and the diffused mode fostering creativity and insight.

### According to the video, what are the steps to take when approaching a new topic? Only mention the points. ###



* Deconstruct the Skill: Break down the larger skill into  smaller, more manageable components.
* Learn Enough to Self-Correct: Gain enough foundational knowledge to identify and correct your mistakes while practicing.
* Practice Deliberately: Focus on focused, deliberate practice rather than passive learning.
* Remove Barriers: Eliminate distractions and make practicing the new skill as easy as possible.


### What are some of the actions you can take going forward to improve your learning process? ###

* use promodoro technique
* Use Feynman technique.
* Use diffused mode of technique to be creative.
* Self Testing
* Set SMART Goals