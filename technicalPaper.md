# Scaling #
   * Scaling define as a technology point of view. Its usually refers to the ability of a system, application, or platform to handle increasing amounts of work or growing user demands.

   * Scaling is main two types: 1.Vertical Scaling, 2.Horizontal Scaling 

   ## Vertical Scaling  ## 
   Vertical scaling is the process of adding more resources to a  server. This can be done by adding more CPU, memory, or storage. Vertical scaling is a nothing but simple way to improve performance, but it can be cost.  



   ## Horizontal Scaling ##
   Horizontal scaling is the process of adding more servers to a system. This can be done by adding new servers to a cluster or by using a cloud-based service such as Amazon Web Services (AWS). Horizontal scaling is more complex than vertical scaling, but it is costless as compare to the vertical scaling.
   


   ## Load Balancers ##
* load balancer is a device that distributes incoming traffic across multiple servers. This can improve performance and reliability by ensuring that no single server is overloaded. Load balancers can be used in a variety of applications, including web servers, email servers, and database servers.

* There are two main types of load balancers: hardware load balancers and software load balancers. Hardware load balancers are physical devices that sit between the client and the servers. Software load balancers are programs that run on a server.

---
1. Incase any employee join at the project is going through some performance and scaling issues. before taking the any task recording the scaling issue. First check the stablility and compactability of the project.

1. Assess the current performance bottlenecks and scalability limitations of the project through performance monitoring, profiling, and load testing.
1. Research and analyze the specific requirements and characteristics of the project to determine whether vertical scaling, horizontal scaling, or a combination of both is the most suitable solution.
1. Consider factors such as cost, complexity, resource availability, and anticipated growth projections when making scaling decisions.
1.Collaborate with team members, architects, and stakeholders to design and implement an effective scaling strategy that addresses the project's performance and scalability needs.
 
### references ###
* https://en.wikipedia.org/wiki/Scaling
* https://www.dremio.com/wiki/vertical-scaling/
* https://www.appviewx.com/education-center/load-balancer-and-types/