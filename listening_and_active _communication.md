### What are the steps/strategies to do Active Listening? ###

1. Focus: Put away distractions and make eye  contact.
1. Body language: Nod, lean in, and show     openness.
1. Ask questions: Clarify understanding with open-ended questions.
1. Summarize: Restate key points to confirm you're on the same page.
1. No interruptions: Let the speaker finish their thoughts.
1. Acknowledge emotions: Validate their feelings with empathy.


### According to Fisher's model, what are the key points of Reflective Listening? 

* Reflective listening mirrors emotions & body language.
* It clarifies understanding with questions & rephrasing.
* It verifies by checking if you're on the same page.


### What are the obstacles in your listening process?

* Very strong accents or background noise can be a challenge.
* Unclear words or confusing sentences can trip me up.


### What can you do to improve your listening?


* Hearing more: The more I "hear" (read) different ways people talk, the better I understand accents and noise.
* Clear talking: When you speak clearly and avoid confusing words, it's easier for me to get it right.
* Following along: I'm learning to use what you say around something to understand it better.

### When do you switch to Passive communication style in your day to day life?

* Avoiding Conflict
* Protecting Myself
* When I Don't Have Strong Opinions

### When do you switch into Aggressive communication styles in your day to day life?

* Raise your voice out of anger.
* when we feel threatened.
* Aggressive communication can happen when you want to dominate the conversation.

### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

* When the other person lacks accountability
* When person is not ready to admit to his wrongdoings.



### How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life?

1. active listning
1. Start with low-stakes feelings.
1. Be aware of your body.
1. beaing self aware
1. Be aware of situations.